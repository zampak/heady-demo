package net.demo.heady;

public interface CoreContract {

    void onSuccess();

    void onError(String message);
}
