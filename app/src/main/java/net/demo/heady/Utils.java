package net.demo.heady;

import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.util.Date;


public class Utils {

    private static int backPressCount = 0;
    public static Date lastKeyTime = null;

    public static void backPressApplicationClose(AppCompatActivity activity) {
        backPressCount++;
        Date currentDate = new Date();
        long difference = (currentDate.getTime() - lastKeyTime.getTime()) / 1000;
        if (backPressCount == 1) {
            lastKeyTime = new Date();
            Toast.makeText(activity, "Press again to Exit", Toast.LENGTH_SHORT).show();
            return;
        } else if (backPressCount > 1) {
            if (difference < 3) {
                System.exit(0);
            } else {
                backPressCount = 1;
                Toast.makeText(activity, "Press again to Exit", Toast.LENGTH_SHORT).show();
                lastKeyTime = new Date();
                return;
            }
        }
    }
}
