package net.demo.heady

import android.annotation.SuppressLint
import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import net.demo.heady.db.HeadyModel
import net.demo.heady.model.Categories
import net.demo.heady.model.ProductFilter
import net.demo.heady.model.Products
import net.demo.heady.model.Rankings
import java.text.SimpleDateFormat


fun getVersionName(): String {
    return "Version ${BuildConfig.VERSION_NAME}"
}

fun AppCompatActivity.show(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}


@SuppressLint("SimpleDateFormat")
fun getDateString(dateString: String): String {
    val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    val outputFormat = SimpleDateFormat("dd-MM-yyyy")
    val date = inputFormat.parse(dateString)
    return outputFormat.format(date)
}


fun getSearchProduct(searchKey: String, list: List<Products>): List<Products> {
    if (list.isEmpty())
        return mutableListOf()
    return list.filter { it -> it.name.toLowerCase().contains(searchKey) }
}

fun getFilterProduct(productFilter: List<ProductFilter>, product: List<Products>): List<Products> {
    if (product.isEmpty())
        return mutableListOf()
    val list: MutableList<Products> = mutableListOf()
    val filterProductIds = getFilterProductIds(productFilter)
    for (data in product) {
        if (isProductBelongToThisFilter(data.id, filterProductIds))
            list.add(data)
    }
    return list
}

fun hideKeyboard(context: AppCompatActivity) {
    val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    var view = context.currentFocus
    if (view == null) {
        view = View(context)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun isProductBelongToThisFilter(id: String, productFilterIds: List<String>): Boolean {
    return id in productFilterIds
}

fun getFilterProductIds(productFilter: List<ProductFilter>): List<String> {
    if (productFilter.isEmpty())
        return mutableListOf()
    val list: MutableList<String> = mutableListOf()
    for (data in productFilter) {
        list.add(data.id)
    }
    return list
}


fun getRankingFromDB(activity: AppCompatActivity): List<Rankings> {
    return HeadyModel.getInstance(activity).data.rankings;
}

fun getCategoryFromDB(activity: AppCompatActivity): List<Categories> {
    return HeadyModel.getInstance(activity).data.categories;
}