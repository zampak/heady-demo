package net.demo.heady.db;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import net.demo.heady.network.APIResponse;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;


public class HeadyModel implements Serializable {

    private static final long serialVersionUID = -4047278198824068072L;
    private static String TAG = HeadyModel.class.getSimpleName();
    @SuppressLint("StaticFieldLeak")
    private static HeadyModel model;
    @SuppressLint("StaticFieldLeak")
    private static AppCompatActivity ownActivity;
    private static String KEY = "KEY";
    private static String FILENAME = "HeadyModel.fwd";
    private HashMap<String, HashMap<String, Object>> mList = new HashMap<>();

    private HeadyModel() {
        loadData();
    }

    public static HeadyModel getInstance(AppCompatActivity activity) {
        ownActivity = activity;
        if (model == null) {
            synchronized (HeadyModel.class) {
                model = new HeadyModel();
            }
        }
        return model;
    }

    private void loadData() {
        FileInputStream fis;
        ObjectInputStream ois;
        try {
            fis = ownActivity.openFileInput(FILENAME);
            ois = new ObjectInputStream(fis);

            model = (HeadyModel) ois.readObject();
            ois.close();
            fis.close();
            mList = model.mList;
        } catch (IOException e) {
            Log.e(TAG, "Error while reading the file ");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            Log.e(TAG, "Error while deserializing");
            e.printStackTrace();
        }
    }

    private void persistData() {
        FileOutputStream fos;
        ObjectOutputStream out;
        try {
            fos = ownActivity.openFileOutput(FILENAME, Context.MODE_PRIVATE);

            try {
                out = new ObjectOutputStream(fos);
                out.writeObject(model);
                out.close();
            } catch (IOException e) {
                Log.e(TAG, "Error while writing the file ");
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            Log.e(TAG, "Error while writing the file ");
            e.printStackTrace();
        }
    }

    public void setData(APIResponse data) {
        HashMap<String, Object> entry = new HashMap<>();
        entry.put("data", data);


        mList.put(KEY, entry);
        persistData();
    }


    @SuppressWarnings("unchecked")
    public APIResponse getData() {
        checkIfKeyExists();
        APIResponse item = (APIResponse) Objects.requireNonNull(mList.get(KEY)).get("data");
        if (item != null) {
            return item;
        }
        return new APIResponse();
    }

    private void checkIfKeyExists() {
        if (!mList.containsKey(KEY)) {
            HashMap<String, Object> entry = new HashMap<>();
            entry.put("data", null);
            mList.put(KEY, entry);
        }
    }
}
