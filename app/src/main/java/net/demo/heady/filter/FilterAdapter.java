package net.demo.heady.filter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.demo.heady.R;
import net.demo.heady.model.Rankings;

import java.util.ArrayList;
import java.util.List;

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.HeroViewHolder> {

    private ManageFilterContract contract;
    private Context mCtx;
    private List<Rankings> mList = new ArrayList<>();

    FilterAdapter(Context mCtx, List<Rankings> mList, ManageFilterContract contract) {
        this.mList = mList;
        this.contract = contract;
        this.mCtx = mCtx;
    }

    @NonNull
    @Override
    public HeroViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.filter_row, parent, false);
        return new HeroViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HeroViewHolder holder, int position) {
        Rankings data = mList.get(position);
        holder.filterTitle.setText(data.getRanking());
        holder.constraint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contract.onFilterCategoryClick(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    static class HeroViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout constraint;
        TextView filterTitle;

        HeroViewHolder(View itemView) {
            super(itemView);
            constraint = itemView.findViewById(R.id.constraint);
            filterTitle = itemView.findViewById(R.id.filterTitle);
        }
    }
}
