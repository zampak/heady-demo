package net.demo.heady.filter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.demo.heady.R;
import net.demo.heady.model.Rankings;

import java.util.List;

import static net.demo.heady.UtilsManagerKt.getRankingFromDB;

public class FilterBottomSheet extends BottomSheetDialogFragment implements ManageFilterContract {

    private TextView clearFilterTextView;

    private ManageFilterContract contract;
    private AppCompatActivity activity;
    private final String TAG = "FilterBottomSheet";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.filter_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = view.findViewById(R.id.filterRecyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(activity, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        List<Rankings> list = getRankingFromDB(activity);
        FilterAdapter adapter = new FilterAdapter(activity, list, this);
        recyclerView.setAdapter(adapter);


        view.findViewById(R.id.clearFilterTextView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contract != null) {
                    contract.clearFilter();
                    FilterBottomSheet.this.dismiss();
                }
            }
        });
    }

    public void setManageFilterContract(AppCompatActivity activity, ManageFilterContract contract) {
        this.contract = contract;
        this.activity = activity;
        this.show(activity.getSupportFragmentManager(), TAG);
    }

    @Override
    public void onFilterCategoryClick(Rankings data) {
        if (contract != null) {
            contract.onFilterCategoryClick(data);
            this.dismiss();
        }
    }

    // TODO NO USE
    @Override
    public void clearFilter() {

    }
}
