package net.demo.heady.filter;

import net.demo.heady.model.Rankings;

public interface ManageFilterContract {
    void onFilterCategoryClick(Rankings data);

    void clearFilter();
}
