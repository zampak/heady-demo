package net.demo.heady.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Categories implements Serializable {
    private String id;
    private String name;
    private List<Products> products = new ArrayList<>();
    private List<String> child_categories = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Products> getProducts() {
        return products;
    }

    public void setProducts(List<Products> products) {
        this.products = products;
    }

    public List<String> getChild_categories() {
        return child_categories;
    }

    public void setChild_categories(List<String> child_categories) {
        this.child_categories = child_categories;
    }
}
