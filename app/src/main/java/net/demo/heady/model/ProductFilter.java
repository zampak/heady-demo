package net.demo.heady.model;

import java.io.Serializable;

public class ProductFilter implements Serializable {
    private String id;
    private String view_count;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getView_count() {
        return view_count;
    }

    public void setView_count(String view_count) {
        this.view_count = view_count;
    }
}
