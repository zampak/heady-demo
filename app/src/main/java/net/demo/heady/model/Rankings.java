package net.demo.heady.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Rankings implements Serializable {
    private String ranking;
    private List<ProductFilter> products = new ArrayList<>();

    public String getRanking() {
        return ranking;
    }

    public void setRanking(String ranking) {
        this.ranking = ranking;
    }

    public List<ProductFilter> getProducts() {
        return products;
    }

    public void setProducts(List<ProductFilter> products) {
        this.products = products;
    }
}
