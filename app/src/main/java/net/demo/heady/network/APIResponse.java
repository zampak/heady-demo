package net.demo.heady.network;

import net.demo.heady.model.Categories;
import net.demo.heady.model.Rankings;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class APIResponse implements Serializable {
    private List<Categories> categories = new ArrayList<>();
    private List<Rankings> rankings = new ArrayList<>();

    public List<Categories> getCategories() {
        return categories;
    }

    public void setCategories(List<Categories> categories) {
        this.categories = categories;
    }

    public List<Rankings> getRankings() {
        return rankings;
    }

    public void setRankings(List<Rankings> rankings) {
        this.rankings = rankings;
    }
}
