package net.demo.heady.network;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.v7.app.AppCompatActivity;

import net.demo.heady.db.HeadyModel;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;


public class CategoryViewModel extends ViewModel {

    private MutableLiveData<APIResponse> heroList;
    private MutableLiveData<String> onError;
    private AppCompatActivity activity;

    public LiveData<APIResponse> fetchData(AppCompatActivity activity) {
        this.activity = activity;
        if (heroList == null) {
            heroList = new MutableLiveData<APIResponse>();
            loadData();
        }
        return heroList;
    }

    public LiveData<String> onError() {
        if (onError == null) {
            onError = new MutableLiveData<String>();
            loadData();
        }
        return onError;
    }

    private void loadData() {
        Call<APIResponse> authVOCall = HeadyAPI.getAuthService().getData();
        authVOCall.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse> call, @NotNull retrofit2.Response<APIResponse> response) {
                APIResponse object = response.body();
                if (object != null) {
                    HeadyModel.getInstance(activity).setData(object);
                    heroList.setValue(response.body());
                } else
                    onError.setValue(call.toString());
            }

            @Override
            public void onFailure(@NotNull Call<APIResponse> call, @NotNull Throwable t) {
                onError.setValue(call.toString());
            }
        });
    }
}
