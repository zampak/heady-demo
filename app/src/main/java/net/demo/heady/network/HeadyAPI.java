package net.demo.heady.network;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public class HeadyAPI {

    private static String BASE_URL = "https://stark-spire-93433.herokuapp.com/";

    public HeadyAPI(String baseUrl) {
        BASE_URL = baseUrl;
    }

    private static AuthService authService = null;

    static synchronized AuthService getAuthService() {
        if (authService == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            authService = retrofit.create(AuthService.class);
        }
        return authService;
    }

    public interface AuthService {

        @GET("json")
        Call<APIResponse> getData();

    }
}
