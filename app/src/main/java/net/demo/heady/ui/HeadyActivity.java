package net.demo.heady.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;

import net.demo.heady.R;
import net.demo.heady.Utils;
import net.demo.heady.ui.category.CategoryFragment;
import net.demo.heady.ui.detail.ProductDetailFragment;
import net.demo.heady.ui.product.ProductFragment;

import java.util.Date;

public class HeadyActivity extends AppCompatActivity {

    private final String TAG = "CategoryActivity";

    public ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Utils.lastKeyTime = new Date();
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        CategoryFragment fragment = new CategoryFragment();
        ft.replace(R.id.containerFrameLayout, fragment);
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.containerFrameLayout);
        if (fragment instanceof CategoryFragment)
            Utils.backPressApplicationClose(this);
        else if (fragment instanceof ProductFragment)
            getSupportFragmentManager().popBackStack();
        else if (fragment instanceof ProductDetailFragment)
            getSupportFragmentManager().popBackStack();
        else
            Utils.backPressApplicationClose(this);
    }
}
