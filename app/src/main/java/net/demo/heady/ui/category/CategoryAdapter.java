package net.demo.heady.ui.category;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import net.demo.heady.R;
import net.demo.heady.model.Categories;

import java.util.ArrayList;
import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.HeroViewHolder> {

    private CategoryContract contract;
    private Context mCtx;
    private List<Categories> mList = new ArrayList<>();

    CategoryAdapter(Context mCtx, CategoryContract contract) {
        this.contract = contract;
        this.mCtx = mCtx;
    }

    void loadData(List<Categories> mList) {
        this.mList = mList;
    }

    @NonNull
    @Override
    public HeroViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.category_row, parent, false);
        return new HeroViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HeroViewHolder holder, int position) {
        Categories data = mList.get(position);
        Glide.with(mCtx)
                .load(R.mipmap.ic_placeholder)
                .into(holder.categoryImageView);
        holder.titleTextView.setText(data.getName());
        holder.categoryImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contract.onClick(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    static class HeroViewHolder extends RecyclerView.ViewHolder {
        ImageView categoryImageView;
        TextView titleTextView;

        HeroViewHolder(View itemView) {
            super(itemView);
            categoryImageView = itemView.findViewById(R.id.categoryImageView);
            titleTextView = itemView.findViewById(R.id.titleTextView);
        }
    }
}
