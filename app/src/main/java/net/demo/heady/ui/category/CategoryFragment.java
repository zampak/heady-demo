package net.demo.heady.ui.category;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.Group;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import net.demo.heady.Constants;
import net.demo.heady.R;
import net.demo.heady.model.Categories;
import net.demo.heady.network.APIResponse;
import net.demo.heady.network.CategoryViewModel;
import net.demo.heady.ui.HeadyActivity;
import net.demo.heady.ui.product.ProductFragment;

import java.util.List;

import static net.demo.heady.UtilsManagerKt.getCategoryFromDB;

public class CategoryFragment extends Fragment implements CategoryContract {

    FrameLayout noDataFrameLayout;
    RecyclerView recyclerView;
    Group groupSort;

    private HeadyActivity activity;
    private CategoryAdapter adapter;
    private CategoryViewModel model;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.list_layout, parent, false);
        activity = (HeadyActivity) getActivity();
        assert activity != null;
        model = ViewModelProviders.of(this).get(CategoryViewModel.class);
        manageUI(v);
        manageAPICall();
        return v;
    }

    private void manageAPICall() {
        if (getCategoryFromDB(activity).size() == 0)
            callAPI();
        else
            setAdapter(getCategoryFromDB(activity));
    }

    private void callAPI() {
        activity.progressBar.setVisibility(View.VISIBLE);

        // THIS IS FOR GET DATA
        model.fetchData(activity).observe(this, new Observer<APIResponse>() {
            @Override
            public void onChanged(@Nullable APIResponse apiResponse) {
                activity.progressBar.setVisibility(View.GONE);
                if (apiResponse != null && apiResponse.getCategories() != null) {
                    setAdapter(apiResponse.getCategories());
                } else
                    manageEmpty(true);
            }
        });

        // THIS IS FOR ERROR
        model.onError().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                activity.progressBar.setVisibility(View.GONE);
                Toast.makeText(activity, s, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void manageEmpty(boolean isEmpty) {
        if (isEmpty) {
            noDataFrameLayout.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            noDataFrameLayout.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    private void manageUI(View view) {
        groupSort = view.findViewById(R.id.groupSort);
        noDataFrameLayout = view.findViewById(R.id.noDataFrameLayout);
        recyclerView = view.findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new GridLayoutManager(activity, 3);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new CategoryAdapter(activity, this);
        groupSort.setVisibility(View.GONE);
    }


    private void setAdapter(List<Categories> list) {
        manageEmpty(list.size() == 0);
        if (adapter != null) {
            adapter.notifyDataSetChanged();
            adapter.loadData(list);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(Categories data) {
        if (data.getProducts().size() > 0) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.DATA_TAG, data);
            FragmentManager fm = activity.getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ProductFragment fragment = new ProductFragment();
            ft.addToBackStack(Constants.CATEGORY_TAG);
            fragment.setArguments(bundle);
            ft.replace(R.id.containerFrameLayout, fragment);
            ft.commit();
        } else
            Toast.makeText(activity, data.getName() + " " + getString(R.string.no_product), Toast.LENGTH_SHORT).show();
    }
}
