package net.demo.heady.ui.detail;

import net.demo.heady.model.Variants;

public interface ProductDetailContract {

    void onVariantClick(Variants data);
}
