package net.demo.heady.ui.detail;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import net.demo.heady.Constants;
import net.demo.heady.R;
import net.demo.heady.model.Categories;
import net.demo.heady.model.Products;
import net.demo.heady.model.Variants;
import net.demo.heady.ui.HeadyActivity;

import java.util.List;

public class ProductDetailFragment extends Fragment implements ProductDetailContract {

    TextView taxValueTextView;
    TextView productCategoryTextView;
    TextView productNameTextView;
    TextView taxNameTextView;
    RecyclerView variantRecyclerView;

    TextView addCartTextView;
    CardView addToCartView;
    String message = "";

    private HeadyActivity activity;
    private VariantAdapter adapter;
    private Products products;
    private Categories category;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.detail_layout, parent, false);
        activity = (HeadyActivity) getActivity();
        assert activity != null;

        Bundle bundle = getArguments();
        if (bundle != null) {

            if (bundle.containsKey(Constants.DATA_TAG))
                products = (Products) bundle.getSerializable(Constants.DATA_TAG);

            if (bundle.containsKey(Constants.DATACATEGORY_TAG))
                category = (Categories) bundle.getSerializable(Constants.DATACATEGORY_TAG);
        }
        manageUI(v);
        manageData();
        return v;
    }

    private void manageData() {

        // CATEGORY
        productCategoryTextView.setText(category.getName());
        productNameTextView.setText(products.getName());

        // TAX
        taxNameTextView.setText(products.getTax().getName());
        taxValueTextView.setText(products.getTax().getValue());

        addToCartView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (message.length() == 0)
                    Toast.makeText(activity, R.string.buy_message, Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(activity, getString(R.string.need_to) + " " + message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void manageUI(View view) {
        productNameTextView = view.findViewById(R.id.productNameTextView);

        productCategoryTextView = view.findViewById(R.id.productCategoryTextView);

        addCartTextView = view.findViewById(R.id.addCartTextView);
        addToCartView = view.findViewById(R.id.addToCartView);

        taxNameTextView = view.findViewById(R.id.taxNameTextView);
        taxValueTextView = view.findViewById(R.id.taxValueTextView);

        variantRecyclerView = view.findViewById(R.id.variantRecyclerView);
        LinearLayoutManager layoutManager = new GridLayoutManager(activity, 4);
        variantRecyclerView.setLayoutManager(layoutManager);
        adapter = new VariantAdapter(activity, this);
        setAdapter(products.getVariants());
    }

    private void setAdapter(List<Variants> list) {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
            adapter.loadData(list);
            variantRecyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onVariantClick(Variants data) {
        message = getString(R.string.pay) + " " + data.getPrice();
        addCartTextView.setText(message);
    }

}
