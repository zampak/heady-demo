package net.demo.heady.ui.detail;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import net.demo.heady.R;
import net.demo.heady.model.Variants;

import java.util.ArrayList;
import java.util.List;

public class VariantAdapter extends RecyclerView.Adapter<VariantAdapter.HeroViewHolder> {

    private ProductDetailContract contract;
    private Context mCtx;
    private List<Variants> mList = new ArrayList<>();

    VariantAdapter(Context mCtx, ProductDetailContract contract) {
        this.contract = contract;
        this.mCtx = mCtx;
    }

    void loadData(List<Variants> mList) {
        this.mList = mList;
    }

    @NonNull
    @Override
    public HeroViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.product_variant_row, parent, false);
        return new HeroViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HeroViewHolder holder, int position) {
        Variants data = mList.get(position);
        holder.productColorTextView.setText(data.getColor());
        holder.radioButton.setText(data.getSize());


        if (data.isSelected()) {
            holder.radioButton.setChecked(true);
            holder.radioButton.setBackgroundResource(R.drawable.state_checked);
        } else {
            holder.radioButton.setChecked(false);
            holder.radioButton.setBackgroundResource(R.drawable.radio_background);
        }

        holder.radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (data.isSelected()) {
                    data.setSelected(false);
                } else {
                    data.setSelected(true);
                }
                reset(position);
                mList.set(position, data);
                notifyDataSetChanged();
                contract.onVariantClick(data);
            }
        });
    }

    private void reset(int pos) {
        for (int i = 0; i < mList.size(); i++) {
            Variants data = mList.get(i);
            if (pos != i)
                data.setSelected(false);
            mList.set(i, data);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    static class HeroViewHolder extends RecyclerView.ViewHolder {
        TextView productColorTextView;
        RadioButton radioButton;

        HeroViewHolder(View itemView) {
            super(itemView);
            productColorTextView = itemView.findViewById(R.id.productColorTextView);
            radioButton = itemView.findViewById(R.id.radioButton);
        }
    }
}
