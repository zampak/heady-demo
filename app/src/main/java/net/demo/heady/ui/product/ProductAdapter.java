package net.demo.heady.ui.product;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import net.demo.heady.R;
import net.demo.heady.model.Products;

import java.util.ArrayList;
import java.util.List;

import static net.demo.heady.UtilsManagerKt.getDateString;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.HeroViewHolder> {

    private ProductContract contract;
    private Context mCtx;
    private List<Products> mList = new ArrayList<>();

    ProductAdapter(Context mCtx, ProductContract contract) {
        this.contract = contract;
        this.mCtx = mCtx;
    }

    void loadData(List<Products> mList) {
        this.mList = mList;
    }

    @NonNull
    @Override
    public HeroViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.product_row, parent, false);
        return new HeroViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HeroViewHolder holder, int position) {
        Products data = mList.get(position);
        Glide.with(mCtx)
                .load(R.mipmap.ic_placeholder)
                .into(holder.productImageView);
        holder.productNameTextView.setText(data.getName());
        String date = getDateString(data.getDate_added());
        holder.productAddDateTextView.setText(date);
        holder.constraint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contract.onClick(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    static class HeroViewHolder extends RecyclerView.ViewHolder {
        ImageView productImageView;
        TextView productNameTextView;
        TextView productAddDateTextView;
        ConstraintLayout constraint;

        HeroViewHolder(View itemView) {
            super(itemView);
            constraint = itemView.findViewById(R.id.constraint);
            productImageView = itemView.findViewById(R.id.productImageView);
            productNameTextView = itemView.findViewById(R.id.productNameTextView);
            productAddDateTextView = itemView.findViewById(R.id.productAddDateTextView);
        }
    }
}
