package net.demo.heady.ui.product;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.Group;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;

import net.demo.heady.Constants;
import net.demo.heady.R;
import net.demo.heady.filter.FilterBottomSheet;
import net.demo.heady.filter.ManageFilterContract;
import net.demo.heady.model.Categories;
import net.demo.heady.model.Products;
import net.demo.heady.model.Rankings;
import net.demo.heady.ui.HeadyActivity;
import net.demo.heady.ui.detail.ProductDetailFragment;

import java.util.List;

import static net.demo.heady.UtilsManagerKt.getFilterProduct;
import static net.demo.heady.UtilsManagerKt.getSearchProduct;
import static net.demo.heady.UtilsManagerKt.hideKeyboard;

public class ProductFragment extends Fragment implements ProductContract {

    FrameLayout noDataFrameLayout;
    RecyclerView recyclerView;
    EditText searchProductEditText;
    ImageView filterImageView;
    Group groupSort;

    private HeadyActivity activity;
    private ProductAdapter adapter;
    private Categories categories;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.list_layout, parent, false);
        activity = (HeadyActivity) getActivity();
        assert activity != null;

        Bundle bundle = getArguments();
        if (bundle != null) {

            if (bundle.containsKey(Constants.DATA_TAG))
                categories = (Categories) bundle.getSerializable(Constants.DATA_TAG);
        }
        manageUI(v);
        mangeLogic();
        return v;
    }

    private void mangeLogic() {

        // Search Handle here
        searchProductEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 2) {
                    List<Products> list = getSearchProduct(s.toString().toLowerCase(), categories.getProducts());
                    setAdapter(list);
                } else {
                    setAdapter(categories.getProducts());

                    if(s.length()==0)
                        hideKeyboard(activity);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        // Filter Handle here
        filterImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new FilterBottomSheet().setManageFilterContract(activity, new ManageFilterContract() {
                    @Override
                    public void onFilterCategoryClick(Rankings data) {
                        List<Products> list = getFilterProduct(data.getProducts(), categories.getProducts());
                        setAdapter(list);
                    }

                    @Override
                    public void clearFilter() {
                        setAdapter(categories.getProducts());
                    }
                });
            }
        });
    }


    private void manageEmpty(boolean isEmpty) {
        if (isEmpty) {
            noDataFrameLayout.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            noDataFrameLayout.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    private void manageUI(View view) {
        filterImageView = view.findViewById(R.id.filterImageView);
        searchProductEditText = view.findViewById(R.id.searchProductEditText);
        groupSort = view.findViewById(R.id.groupSort);
        noDataFrameLayout = view.findViewById(R.id.noDataFrameLayout);
        recyclerView = view.findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(activity, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ProductAdapter(activity, this);
        setAdapter(categories.getProducts());
        groupSort.setVisibility(View.VISIBLE);
    }


    private void setAdapter(List<Products> list) {
        manageEmpty(list.size() == 0);
        if (adapter != null) {
            adapter.notifyDataSetChanged();
            adapter.loadData(list);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(Products data) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.DATA_TAG, data);
        bundle.putSerializable(Constants.DATACATEGORY_TAG, categories);
        FragmentManager fm = activity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ProductDetailFragment fragment = new ProductDetailFragment();
        ft.addToBackStack(Constants.PRODUCT_TAG);
        fragment.setArguments(bundle);
        ft.replace(R.id.containerFrameLayout, fragment);
        ft.commit();
    }
}
